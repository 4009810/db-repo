## **db-repo (`v0.01`)**

### **docker:**
```sh
git clone https://dev_bruno_felipe_4009810@bitbucket.org/4009810/db-repo.git
cd db-repo/mysql/
docker build -t mysql-image:v0.01 . 
docker run --name mysql-container -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root mysql-image:v0.01
docker logs --follow mysql-container #Exit: Ctrl+c 
docker exec -it mysql-container mysql -uroot -proot usersdb
select * from users where name = 'ToZe';
```

### **docker-compose:**
```sh
git clone https://dev_bruno_felipe_4009810@bitbucket.org/4009810/db-repo.git
cd db-repo/
docker-compose build
docker-compose up -d
docker-compose logs --follow  #Exit: Ctrl+c
docker-compose exec mysql-container mysql -uroot -proot usersdb  
```
